#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game.h"
#include "interface.h"

/******************************************************************************
        Function description: 游戏初始化
        Entry data:     
        Return value: None
******************************************************************************/
void game_init(struct player *p)
{
	if(p == NULL) return;
	char temp[64];
	printf("\33[2J");
	printf("\33[0;0H");
	printf("新君主姓名:");
	scanf("%s",temp);
	memset((void*)p,'\0',sizeof(struct player));	
	if(strlen(temp) < 16)
	{
		strcpy(p->name,temp);
	}
	else
	{
		strcpy(p->name,"NULL");
	}

	p->cc	=	100;
	p->food	=	99999;
	p->gold	=	99999;
	p->edu	=	99;
	p->evi	=	99;
	p->peo 	=	9999;
	p->con 	=	99;
	p->age	=	18;
	p->month=	9;
	p->city	=	9;
	p->tax	=	30;
	p->farm	=	10;
	p->stone=	99;
	p->opeo	=	9999;
	p->jd	=	999;
	p->zdl	=	99;
}

void save(struct player *p)
{
	FILE *fp = NULL;
	fp 	=	fopen("CA","wb");
	if(fp != NULL)
	{
		fwrite((void*)p,sizeof(struct player),1,fp);
		fclose(fp);
	}
}

int load(struct player *p)
{
	FILE *fp 	= fopen("CA","rb");
	if(fp != NULL)
	{
		fread((void*)p,sizeof(struct player),1,fp);
		fclose(fp);
		return 0;
	}
	return 1;
}

int notMetch		=	0;
int menuList[7]		= {0,100,200,250,300,310,320};
int actionList[16]	= {210,220,230,240,251,252,260,311,312,313,314,321,322,323,330,340};
int metch(int index, int opt)	//0-menuMetch  1-actionMecth
{
	int i;
	if(opt == 0)
	{
		for(i = 0; i < 7; i++)
		{
			if(index == menuList[i]){
				notMetch	=	0;
				return 1;
			}
		}
	}
	else if(opt == 1)
	{
		for(i = 0; i < 16; i++)
		{
			if(index == actionList[i]){
				notMetch	=	0;
				return 1;
			}
		}
	}
	notMetch	=	index;
	return 0;
}

/******************************************************************************
        Function description: 操作解码-界面转换
        Entry data:     
        Return value: None
******************************************************************************/
void operateDecode(int *gui_index, int *menu_index, int *action_index, char key)
{
	int gui_temp = 999;
	if(*menu_index == 3 && (key == 'p' || key == 'P'))	return ;
	if(*menu_index == 3)
	{
		//printf("operateDecode\t%d\t%d\r\n",key,100*(key-0x30));
		//if(key == 'q' || key == 'Q')	gui_index	=	0;	//离开
		//if(key == 's' || key == 'S')	gui_index	=	0;	//保存
		if     (key == 'z' || key == 'Z' || key == 0x70)		//状态
		{	
			*gui_index	=	100;
			*menu_index	=	2;
		}
		else if(key == 'x' || key == 'X' || key == 0x71)		//内政
		{
			*gui_index	=	200;
			*menu_index	=	2;
		}
		else if(key == 'c' || key == 'C' || key == 0x72)		//开发
		{
			*gui_index	=	300;
			*menu_index	=	2;
		}
	}
	else if(*menu_index == 2)
	{
		//printf("operateDecode\t%d\t%d\r\n",key,10*(key-0x30));
		if(key >0x30 && key < 0x37)
		{
		
			gui_temp	=	*gui_index + 10*(key-0x30);
			if(metch(gui_temp, METCH_MENU))
			{
				*gui_index	=	gui_temp;
				*menu_index	=	1;
			}
		}
		else if(key == 'p' || key == 'P')
		{
			*gui_index	=	0;
			*menu_index	=	3;
		}
	}
	else if(*menu_index == 1)
	{
		//printf("operateDecode\t%d\t%d\r\n",key,(key-0x30));
		if(key >0x30 && key < 0x36)
		{
			gui_temp	=	*gui_index + (key-0x30);
			if(metch(gui_temp, METCH_MENU))
			{
				*gui_index	=	gui_temp;
				*menu_index	=	0;
			}
		}
		else if(key == 'p' || key == 'P')
		{
			gui_temp	=	(*gui_index/100)*100;
			if(metch(gui_temp, METCH_MENU))
			{
				*gui_index	=	gui_temp;
				*menu_index	=	2;
			}
		}
	}
#if 1
//	printf("notMetch:%d\r\n",notMetch);
//	getchar();
	if(notMetch)
	{
		gui_temp	=	notMetch;
		if(metch(notMetch, METCH_ACTION))
		{
			*action_index	=	gui_temp;
		}
	}
#endif
}

extern int action_index_last;
/******************************************************************************
        Function description: 事件处理机
        Entry data:     
        Return value: None
******************************************************************************/
void action(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int flush_index = 0;
	if(*action_index != ACTION_TAX && *action_index != ACTION_PRODUCE && \
	   *action_index != ACTION_DEEL_M && *action_index != ACTION_DEEL_G)
		flush_index	=	1;

	switch(*action_index)
	{
		case	0:		flush_index = 0; break;
		case	ACTION_TAX:	{tax(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_PRODUCE:	{produce(gui_index,menu_index,action_index,p);		break;}

		case	ACTION_EDUCATE:	{educate(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_VISIT:	{visit(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_DEEL_M:	{deel(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_DEEL_G:	{deel(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_CONSCRIPTION:
					{conscription(gui_index,menu_index,action_index,p);	break;}
		case	ACTION_FARMING:	{farming(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_METALLURGY:
					{metallurgy(gui_index,menu_index,action_index,p);	break;}
		case	ACTION_LOOKING:	{looking(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_SIEGE:	{siege(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_FLOOD:	{flood(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_SALVATION:
					{salvation(gui_index,menu_index,action_index,p);	break;}
		case	ACTION_TEACHING:
					{teaching(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_SEARCH:	{search(gui_index,menu_index,action_index,p);		break;}
		case	ACTION_TRAINING:
					{training(gui_index,menu_index,action_index,p);		break;}
		default:
		{	
			flush_index	=	0;
			*gui_index	=	0;
			*menu_index	=	3;
		}
	}

	if(flush_index)	update(gui_index,menu_index,action_index,p);
	action_index_last	=	*action_index;
	*action_index	=	0;
}

/******************************************************************************
        Function description: 游戏结局处理机
        Entry data:     
        Return value: None
******************************************************************************/
void ending(int eindex, struct player *p)
{
	switch(eindex)
	{
		case 8000:
		{
			printf("很遗憾!!你的人民大多病死或离你而去,你的国家灭亡在西元!\r\n");
			printf("%s\r\n",p->name);
			printf("-%4d年灭亡!!!!\r\n",p->age+202);
			printf("%c\r\n",0x7);
			break;
		}
		case 9010:
		{
			printf("-%4d年\r\n",p->age+202);
			printf("君主：%s 因积劳过度,体力不支,因而与世长辞...\r\n",p->name);
			break;
		}
		case 9900:
		{
			printf("你被敌人打败了!含恨而死!\r\n");
			break;
		}
		case 9910:
		{
			printf("你不负重任,完成了统一中国的大业!你将被载入史册!人民纪念你!\r\n");
			break;
		}
		default: break;
	}

	getchar();
	exit(1);
}

/******************************************************************************
        Function description: 数据更新
        Entry data:     
        Return value: None
******************************************************************************/
void update(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int type,dpeo,n;
	long t;
	if(p->peo < 2)	ending(8000, p);
	p->month	+=	1;
	p->evi 		-=	2;
	if(p->month == 13){ p->age += 1; p->month = 1;}
	if(p->age > 72)	ending(9000, p);

	srand((unsigned) time(&t));
	type	=	rand() % 20 +1;
	if(p->month == 1)
	{
		if(p->food < p->peo *10)
		{
			printf("粮食不足!!!\r\n");
			printf("有些百姓饿死!!!\r\n");
			p->peo 	=	p->food / 10;
			p->con 	-=	7;
			p->food	=	1;
			p->gold	-=	p->peo  * 2;
			if(p->gold < 2) p->gold = 1;
			getchar();
		}
		if(p->gold < (p->peo * 10))
		{
			printf("黄金不足!!!!\r\n");
			printf("有些百姓不满而离开!\r\n");
			srand((unsigned) time(&t));
			p->con 	-=	rand() % 10;
			p->food	-=	p->peo * 10;
			p->peo	-=	p->gold / 2 + 3;
			p->gold = 1;

			srand((unsigned) time(&t));
			n 	=	rand() % 10;
			p->farm	-=	n + 3;
			p->edu	-=	n + 9;
			p->stone-=	n + 2;
			getchar();
		}
		if(p->con < 40)
		{
			printf("人民信心不足!!!!\r\n");
			printf("有些百姓离开本国!!!!\r\n");
			p->peo	-=	p->peo * 0.5 / p ->con + 100;
			p->gold = 1;

			srand((unsigned) time(&t));
			n 	=	rand() % 10;
			p->farm	-=	n + 3;
			p->edu	-=	n + 9;
			p->stone-=	n + 2;
			getchar();
		}
		if(p->peo > (p->city * 1000))
		{
			printf("城池数太少!!!!\r\n");
			printf("生活变的不顺利!!!!\r\n");
			srand((unsigned) time(&t));
			p->evi	-=	rand() % 20 + 10;
			p->gold = 1;

			srand((unsigned) time(&t));
			n 	=	rand() % 10;
			p->peo	=	p->city * 1000 + 3;
			p->city	+=	1;
			srand((unsigned) time(&t));
			p->con	-=	rand() % 10 + 15;
			*gui_index	=	0;
			*menu_index	=	3;
			getchar();
			return ;
		}
	}
	else
	{
		if(type == 19)
			{printf("找到大宝库!!!!!\r\n"); p->gold += 5000; getchar();}
		if(p->tax > 49)
			{printf("税率过高,人民不满!\r\n"); p->con -= 25; getchar();}
	}
//5070
	if(p->evi < 31 && type < 6)
	{
		printf("气候不佳,民生艰困!!\r\n"); 
		p->con	-=	(type+9); p->farm -=  (type + 15);
		dpeo	=	p->peo * (type / 100 + 0.2);
		p->peo	-=	(dpeo + type * 10);
		getchar();
	}

	if(p->evi < 75 && type == 20)
	{
		printf("发生大地震!!!\r\n");
		p->peo		*=	0.6;
		p->con		-=	20;
		p->farm		-=	20;
		p->stone 	-=	44;
		p->evi 		-=	33;
	}
	if(type == 20)
	{
		p->gold	/=	2;
		p->food /=	2;
	}
	srand((unsigned) time(&t));
	p->con -= rand() % 20;

	if(p->cc == 0)	ending(9910, p);

	printf("进入 %d 月!!!\r\n",p->month);
	if(p->con > 99)		p->con		=	100;
	if(p->evi > 99)		p->evi		=	100;
	if(p->farm > 99)	p->farm 	=	100;
	if(p->stone > 99)	p->stone 	=	100;

	if(p->edu < 1)		p->edu		=	1;
	if(p->evi < 1)		p->evi		=	1;
	if(p->con < 1)		p->con		=	1;
	if(p->peo < 1)		p->peo		=	1;
	if(p->farm < 1)		p->farm		=	1;
	if(p->stone < 1)	p->stone	=	1;

	*gui_index	=	0;
	*menu_index	=	3;
}

//start
/******************************************************************************
        Function description: 事件处理机-内政-税收
        Entry data:     
        Return value: None
******************************************************************************/
void tax(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int tax,ncon;
	printf("\33[2J");
	printf("\33[0;0H");
	printf("现行税率:\t%3d%%\r\n",p->tax);
	printf("税收设定:");
	scanf("%d",&tax);
	if(tax > 99) 		p->tax	=	100;
	else if(tax > 1)	p->tax	=	tax;
	else			p->tax	=	1;

	ncon	=	2400/p->tax;
	if(ncon > 99)		ncon 	=	100;
	if(p->con < ncon)	ncon	=	p->con;
	else			p->con 	=	ncon;
	p->con 			=	ncon;

	printf("人民信赖度为:\t%d\r\n",ncon);
	*gui_index	=	200;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-内政-生产
        Entry data:     
        Return value: None
******************************************************************************/
void produce(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int nf,ng,af,ag;
	printf("\33[2J");
	printf("\33[0;0H");
	printf("一分耕耘,一分收获\r\n");
	printf("现在月份:\t%3d%%\r\n",p->tax);
	printf("按任一键进行收割!\r\n");
	getchar();
	if(p->month == 7)
	{
		nf 	=	p->tax * p->city * p->con *p->farm;
		ng	=	p->tax * p->peo * p->stone * p->con /1000;
		af	=	nf * 40 / 130;
		ag 	=	ng * 10 / 143;
		if(p->gold / (ag+1) > 0.2)	ag = p->gold * 0.35;
		if(p->food / (af+1) > 0.2)	af = p->food * 0.35;
		p->food	+=	af;
		p->gold	+=	ag;
		printf("\33[2J");
		printf("\33[0;0H");
		printf("收割完成!!\r\n");
		printf("由人民纳税获得\r\n");
		printf("粮食:\t%d\r\n",af);
		printf("黄金:\t%d\r\n",ag);
	}
	else
	{
		printf("\33[2J");
		printf("\33[0;0H");
		printf("作物尚未成熟!!\r\n");
		printf("等到7月再收割吧!!\r\n");
	}

	*gui_index	=	200;
	*menu_index	=	2;
}


/******************************************************************************
        Function description: 事件处理机-内政-训练(教育)
        Entry data:     
        Return value: None
******************************************************************************/
void educate(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int edu;
	printf("\33[2J");
	printf("\33[0;0H");
	printf("开始进行教育训练!\r\n");
	edu 	=	(20 /p->city + 0.2) + p->edu + 2;
	if(edu > 99)	edu = 100;
	p->edu 	=	edu;
	printf("人民教育程度\r\n");
	printf("上升为:\t%3d%%\r\n",edu);

	*gui_index	=	200;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-内政-访查
        Entry data:     
        Return value: None
******************************************************************************/
void visit(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int con,n;
	printf("\33[2J");
	printf("\33[0;0H");
	printf("访查人民,以获民心!!\r\n");
	getchar();

	n 	=	p->evi * p->peo /1000;
	con 	=	(40 / n * 20) + p->con + 5;
	if(con > 99)	con = 100;
	p->con 	=	con;

	printf("\33[2J");
	printf("\33[0;0H");
	printf("人民信赖度上升为:\t%3d%%\r\n",con);

	*gui_index	=	200;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-内政-贸易
        Entry data:     
        Return value: None
******************************************************************************/
void deel(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int cost=0;
	if(*action_index == DEEL_F2G)
	{
		while(1)
		{
			printf("\33[2J");
			printf("\33[0;0H");
			printf("现有米量:\t%d\r\n",p->food);
			printf("用米多少:");
			scanf("%d",&cost);
			if(cost < p->food)
			{
				p->food	-=	cost;
				p->gold	+=	cost/50;
				printf("获得金量:\t%d\r\n",cost/50);
				*gui_index	=	200;
				*menu_index	=	2;
				break;
			}
		}
	}
	else if(*action_index == DEEL_G2F)
	{
		while(1)
		{
			printf("\33[2J");
			printf("\33[0;0H");
			printf("现有金量:\t%d\r\n",p->gold);
			printf("用金多少:");
			scanf("%d",&cost);
			if(cost < p->food)
			{
				p->food	+=	cost*50;
				p->gold	-=	cost;
				printf("获得米量:\t%d\r\n",cost*50);
				*gui_index	=	200;
				*menu_index	=	2;
				break;
			}
		}
	}
	getchar();
}

/******************************************************************************
        Function description: 事件处理机-内政-征兵
        Entry data:     
        Return value: None
******************************************************************************/
void conscription(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int jj;

	while(1)
	{
		printf("\33[2J");
		printf("\33[0;0H");
		printf("输入征兵数量\r\n");
		scanf("%d",&jj);
		if(p->peo > jj && jj > 1 && p->food > jj * 2) break;
		//if(p->peo > jj * 2)printf("粮食不足\r\n"); // add
		//getchar();
	}

	if(jj > (p->peo / 4) && p->con < 80)
	{
		printf("百姓严重不满，奋起反抗！\r\n");
		p->con	=	2;
		p->peo	-=	jj;
		p->jd	+=	jj;
		p->con	-=	3;
		p->zdl	*=	0.85;
		p->food	-=	jj * 2;
		printf("军队数: %d 人\r\n", p->jd);
		getchar();
	}

	*gui_index	=	200;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-行动研发-重犁与铁器（农耕技术）
        Entry data:     
        Return value: None
******************************************************************************/
void farming(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int cost,afarm;
	if(p->gold < 500)	return ;
	while(1)
	{
		printf("\33[2J");
		printf("\33[0;0H");
		printf("【农耕技术】\r\n");
		printf("现有黄金:\t%d\r\n",p->gold);
		printf("用金多少:");
		scanf("%d",&cost);

		if(cost < p->gold) break;
	}
	if(cost < 500)	cost	=	500;
	if(cost > 4999)	cost	=	5000;
	afarm 	=	cost * p->edu /5000 + 3;
	if(afarm > 49)	afarm = 50;
	p->gold	-=	cost;
	p->farm +=	afarm;
	//if(p->farm >99)	p->farm =	100;

	printf("农业技术提升:\t%3d%%\r\n",afarm);

	*gui_index	=	300;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-行动研发-冶金术（采矿技术）
        Entry data:     
        Return value: None
******************************************************************************/
void metallurgy(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int cost,astone;
	if(p->gold < 500)	return ;
	while(1)
	{
		printf("\33[2J");
		printf("\33[0;0H");
		printf("【采矿技术】\r\n");
		printf("现有黄金:\t%d\r\n",p->gold);
		printf("用金多少:");
		scanf("%d",&cost);

		if(cost < p->gold) break;
	}
	if(cost < 500)	cost	=	500;
	if(cost > 4999)	cost	=	5000;
	astone 	=	cost * p->edu /9000 + 3;
	if(astone > 49)	astone = 50;
	p->gold		-=	cost;
	p->stone	+=	astone;
	//if(p->stone >99)	p->stone =	100;

	printf("采矿技术提升:\t%3d%%\r\n",astone);

	*gui_index	=	300;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-行动研发-寻找新城
        Entry data:     
        Return value: None
******************************************************************************/
void looking(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int cost;
	if(p->gold < 500)	return ;

	cost	=	50000000 / p->evi / p->edu;
	{
		printf("\33[2J");
		printf("\33[0;0H");
		printf("【寻找新城池】\r\n");
		printf("配合现况,加以搜寻!\r\n");
		printf("需用金:\t%d\r\n",cost);
	}

	if(cost > p->gold)
	{
		printf("金量不足!!\r\n");
	}
	else
	{
		printf("金量不足!!\r\n");
		p->gold	-=	cost;
		p->city	+=	1;
	}

	*gui_index	=	300;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-行动研发-攻占新城
        Entry data:     
        Return value: None
******************************************************************************/
void siege(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int dj,dz;
	long t;char ch=0;
	printf("\33[2J");
	printf("\33[0;0H");
	printf("是否与敌迎战?\r\n");
	
	

	ch 	=	getchar();
	srand((unsigned) time(&t));
	if(p->cc < 50)	{	dj = rand() % 10 * p->jd; \
				dz = dj / 3 + (p->zdl /1.3);}
	else		{	dj = (rand() %10) * p->jd * 0.6;\
				dz = dj / 8 * (rand() %5) + (p->zdl / 1.7);}

	if(ch  == 'n')
	{
		printf("你弃城投降了！\r\n");
		p->city	-=	1;
		p->cc 	+=	1;
		getchar();
		if(p->city < 1)	ending(9900,p);
		if(p->cc   < 1)	ending(9910,p);
		return ;
	}

	while(1)
	{
		if(p->city < 1)	ending(9900,p);
		printf("战斗中...\r\n");
		printf("我军: %d \r\n",p->jd);
		printf("敌军: %d \r\n",dj);
		p->jd	-=	dz;
		dj	-=	p->zdl;
		if(p->jd < 1)
		{
			printf("你败了!\r\n");
			printf("失去城池!\r\n");
			p->city	-=	1;
			p->cc 	+=	1;
			if(p->city < 1)	ending(9900,p);
			if(p->cc   < 1)	ending(9910,p);
			break;
		}
		if(dj < 1)
		{
			printf("你赢了!\r\n");
			//printf("获得城池!\r\n");
			p->city	+=	1;
			p->cc 	-=	1;
			if(p->city < 1)	ending(9900,p);
			if(p->cc   < 1)	ending(9910,p);
			break;
		}

	}


	*gui_index	=	300;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-环境治理-洪水防治
        Entry data:     
        Return value: None
******************************************************************************/
void flood(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int cost,aevi;
	if(p->gold < 500)	return ;
	printf("\33[2J");
	printf("\33[0;0H");
	printf("现有黄金:\t%d\r\n",p->gold);
	printf("用金多少:");
	scanf("%d",&cost);
	getchar();

	if(cost < 500)	cost	=	500;
	if(cost > 4999)	cost	=	5000;
	aevi 	=	cost * p->edu /8000 + 5;
	if(aevi > 49)	aevi = 50;
	p->gold	-=	cost;
	p->evi 	+=	aevi;
	if(p->evi >99)	p->evi 	=	100;

	printf("环境指数上升:\t%3d%%\r\n",aevi);

	*gui_index	=	300;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-环境治理-赈灾救民
        Entry data:     
        Return value: None
******************************************************************************/
void salvation(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int cost,con;

	while(1)
	{
		printf("\33[2J");
		printf("\33[0;0H");
		printf("现有粮食:\t%d\r\n",p->food);
		printf("用粮多少:");
		scanf("%d",&cost);
		if(cost < p->food)	break;
	}

	if(cost < 500)	cost	=	500;
	con 	=	cost /500 + p->con;
	if(con > 99)	con = 100;
	p->gold	-=	cost;
	p->con 	=	con;

	printf("人民信心上升为:\t%3d%%\r\n",con);

	*gui_index	=	300;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-环境治理-兴办学业
        Entry data:     
        Return value: None
******************************************************************************/
void teaching(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int cost,edu;
	if(p->gold < 500)	return ;
	while(1)
	{
		printf("\33[2J");
		printf("\33[0;0H");
		printf("【兴办学业】\r\n");
		printf("现有黄金:\t%d\r\n",p->gold);
		printf("用金多少:");
		scanf("%d",&cost);

		if(cost < p->gold) break;
	}
	if(cost < 500)	cost	=	500;
	if(cost > 4999)	cost	=	5000;
	edu 	=	cost * p->edu /1000;
	if(edu > 99)	edu = 100;
	p->gold	-=	cost;
	p->edu	+=	edu;

	printf("教育程度上升为:\t%3d%%\r\n",edu);

	*gui_index	=	300;
	*menu_index	=	2;
}


/******************************************************************************
        Function description: 事件处理机-开发-搜寻
        Entry data:     
        Return value: None
******************************************************************************/
void search(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int type,aevi;
	long t;

	printf("\33[2J");
	printf("\33[0;0H");
	printf("--------搜寻--------\r\n");
	printf("从城内找出一些宝物!\r\n");
	printf("按任一键开始!!\r\n");
	getchar();

	srand((unsigned) time(&t));
	type	=	rand() % 20 +1;

	if(type == 1)
	{
		printf("找到一个可以保护,攻击的空城!!\r\n");
		p->city	+=	1;
	}
	else if(type > 1 && type <9)
	{
		printf("找到宝箱!!\r\n");
		printf("内有黄金 %d 两\r\n",type*10);
		p->gold	+=	type*10;
	}
	else if(type > 8)
	{
		printf("发现多余的粮食!!\r\n");
		printf("找到 %d 单位的食物!\r\n",type*50);
		p->food	+=	type*50;
	}

	

	*gui_index	=	300;
	*menu_index	=	2;
}

/******************************************************************************
        Function description: 事件处理机-开发-训练军队
        Entry data:     
        Return value: None
******************************************************************************/
void training(int *gui_index, int *menu_index,int *action_index,struct player *p)
{
	int zdl;
	printf("\33[2J");
	printf("\33[0;0H");
	//printf("开始进行军队训练!\r\n");

	if(p->gold < 1501 || p->food < (p->jd * 2))
	{
		printf("训练成功!\r\n");
		if(p->cc == 0)	ending(9910,p);
	}
	else
	{
		p->gold	-=	1500;
		p->food -=	p->jd  * 2;
		zdl 	=	p->edu * 1.6;
		printf("训练成功!\r\n");
	}

	*gui_index	=	300;
	*menu_index	=	2;
}

