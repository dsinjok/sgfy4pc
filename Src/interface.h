#ifndef	_INTERFACE_H
#define	_INTERFACE_H

#ifdef WIN32
#define usleep(x) Sleep((x)/1000000)
#else
#include <unistd.h>
#endif

//前级菜单
#define	ABOUT			1
#define	COMMENT			2
#define	START			3
#define	OPERATE			4

//主菜单
#define	MAIN_GAME		0


//一级菜单
#define	GAME_OVERVIEW	100
#define	GAME_CHIEF		200
#define	GAME_DEVELOP	300

//二级菜单
#define	CHIEF_TAX		210
#define	CHIEF_PRODU		220
#define	CHIEF_EDU		230
#define	CHIEF_VISIT		240
#define	CHIEF_DEEL		250

#define	DEVELOP_ACT		310
#define	DEVELOP_EVI		320
#define	DEVELOP_SEARCH	330
#define	DEVELOP_TRAIN	340

//三级菜单
#define	DEEL_F2G		251
#define	DEEL_G2F		252

#define	EVI_FLOOD		321
#define	EVI_SLOVE		322
#define	EVI_FOUND		323

//====================== funtion ======================
void interface(int gui_index, int menu_index,struct player *p);
void gui_about(void);
void gui_comment(void);
void gui_start(void);
void gui_operate(void);
void gui_end(void);
void gui_gamemain(struct player *p);
void gui_overview(struct player *p);
void gui_chief(void);
void gui_chief_deel(void);
void gui_develop(void);
void gui_develop_act(void);
void gui_develop_evi(void);

void debug(int *gui_index, int *menu_index, int *action_index, char key);

#endif
