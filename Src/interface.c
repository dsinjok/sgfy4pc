#include <stdio.h>
#include <string.h>
#include "game.h"
#include "interface.h"

/******************************************************************************
        Function description: gui
        Entry data:     gui_index,menu_index
        Return value: None
******************************************************************************/
void interface(int gui_index, int menu_index,struct player *p)
{
	switch(gui_index)
	{
		case	MAIN_GAME:			gui_gamemain(p);	break;
		case	GAME_OVERVIEW:		gui_overview(p);	break;
		case	GAME_CHIEF:			gui_chief();		break;
			//case	CHIEF_TAX:		gamemain(p);	break;
			//case	CHIEF_PRODU:		gamemain(p);	break;
			//case	CHIEF_EDU:		gamemain(p);	break;
			//case	CHIEF_VISIT:		gamemain(p);	break;
			case	CHIEF_DEEL:		gui_chief_deel();	break;
				//case	DEEL_F2G:		gamemain(p);	break;
				//case	DEEL_G2F:		gamemain(p);	break;	

		case	GAME_DEVELOP:		gui_develop();	break;
			case	DEVELOP_ACT:		gui_develop_act();	break;
			case	DEVELOP_EVI:		gui_develop_evi();	break;
				//case	EVI_FLOOD:		gamemain(p);	break;
				//case	EVI_SLOVE:		gamemain(p);	break;
				//case	EVI_FOUND:		gamemain(p);	break;
			//case	DEVELOP_SEARCH:		gamemain(p);	break;
			//case	DEVELOP_TRAIN:		gamemain(p);	break;
		default:	break;

	}
}

/******************************************************************************
		Menu Index:	1
        Function description: 游戏介绍
        Entry data:     
        Return value: None
******************************************************************************/
void gui_about(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("    【三国风云】\r\n");
	printf("程序汉化:Andy\r\n");
	printf("程序版本:1.1.2136\r\n");
	printf("发布日期:2001/03/25\r\n");
	usleep(3000000);
}

/******************************************************************************
		Menu Index:	2
        Function description: 过场动画、背景介绍
        Entry data:     
        Return value: None
******************************************************************************/
void gui_comment(void)
{
	int i;
	char *com1 = "时间:西元220年一月  地点:中国-四川省    人物:自立为王的你   环境:混乱的三国时代\r\n";
	char *com2 = "你须在你有生之年中,扮演国君角色,发展你的国家, 一统全国!\r\n";
	printf("\33[2J");
	printf("\33[0;0H");
	for(i = 0; i < strlen(com1); i++)
	{
		printf("%c",com1[i]);
		usleep(50000);
		fflush(stdout);
	}
	for(i = 0; i < strlen(com2); i++)
	{
		printf("%c",com2[i]);
		usleep(50000);
		fflush(stdout);
	}
}

/******************************************************************************
		Menu Index:	3
        Function description: 游戏选择界面
        Entry data:     
        Return value: None
******************************************************************************/
void gui_start(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("------三国风云------\r\n");
	printf("1.新的游戏\r\n");
	printf("2.旧的记忆\r\n");
	printf("3.退出游戏\r\n");
}

/******************************************************************************
		Menu Index:	4
        Function description: 按键说明
        Entry data:     
        Return value: None
******************************************************************************/
void gui_operate(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("<按键说明>\r\n");
	printf("[F1]状态[S]存档\r\n");
	printf("[F2]内政[Q]离开\r\n");
	printf("[F3]开发\r\n");
}

/******************************************************************************
		Menu Index:	5
        Function description: 退出游戏界面
        Entry data:     
        Return value: None
******************************************************************************/
extern int notMetch;
void gui_end(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("谢谢您的使用!!     程式设计:GPC & GYC  Http://www.taconet  .com.tw/gpc\r\n");
}

void debug(int *gui_index, int *menu_index, int *action_index, char key)
{
	printf("\33[20;5H");
	printf("gui_index:%4d\tmenu_index:%4d\taction_index:%4d\terro:%4d\r\nkey:%c\r\n",\
		*gui_index,*menu_index,*action_index,notMetch,key);
}

/******************************************************************************
		Menu Index:	0
        Function description: 游戏主界面
        Entry data:     
        Return value: None
******************************************************************************/
void gui_gamemain(struct player *p)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("    【三国风云】\r\n");
	printf("君主\t:\t%s\r\n",p->name);
	printf("城池数\t:\t%d\r\n",p->city);
	printf("西元 %d 年 %d 月\r\n",202+p->age,p->month);
}

/******************************************************************************
		Menu Index:	100
        Function description: 状态界面	F1
        Entry data:     
        Return value: None
******************************************************************************/
void gui_overview(struct player *p)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("君主\t:\t%s\r\n",p->name);
	printf("年龄\t:\t%d\r\n",p->age);
	printf("粮食量\t:\t%d\r\n",p->food);
	printf("黄金量\t:\t%d\r\n",p->gold);
	printf("战斗力\t:\t%d\r\n",p->zdl);
	printf("统一度\t:\t%-3d %%\r\n",101-p->cc);
	printf("税率\t:\t%-3d %%\r\n",p->tax);
	printf("农场开发率:\t%-3d %%\r\n",p->farm);
	printf("矿场开发率:\t%-3d %%\r\n",p->stone);
	printf("人口数\t:\t%d 人\r\n",p->peo);
	printf("信赖度\t:\t%-3d %%\r\n",p->con);
	printf("教育程度:\t%-3d %%\r\n",p->edu);
	printf("环境程度:\t%-3d %%\r\n",p->evi);
	printf("军队数\t:\t%d\r\n",p->jd);
}

/******************************************************************************
		Menu Index:	200
        Function description: 内政界面	F2
        Entry data:     
        Return value: None
******************************************************************************/
void gui_chief(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("--------内政--------\r\n");
	printf("1.税收     2.生产\r\n");
	printf("3.教育     4.访查\r\n");
	printf("5.贸易     6.征兵\r\n");
}

/******************************************************************************
		Menu Index:	250
        Function description: 内政-贸易界面[5]
        Entry data:     
        Return value: None
******************************************************************************/
void gui_chief_deel(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("【商业贸易】\r\n");
	printf("物价: 1:50\r\n");
	printf("1.米换金\r\n");
	printf("2.金换米\r\n");
}

/******************************************************************************
		Menu Index:	300
        Function description: 开发界面	F3
        Entry data:     
        Return value: None
******************************************************************************/
void gui_develop(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("--------开发--------\r\n");
	printf("1.行动研发\r\n");
	printf("2.环境治理\r\n");
	printf("3.搜寻\r\n");
	printf("4.训练军队\r\n");
}

/******************************************************************************
		Menu Index:	310
        Function description: 开发-行动研发界面 [1]
        Entry data:     
        Return value: None
******************************************************************************/
void gui_develop_act(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("------行动研发------\r\n");
	printf("1.重犁及铁器\r\n");
	printf("2.冶金术\r\n");
	printf("3.寻找新城\r\n");
	printf("4.攻占敌城\r\n");
}

/******************************************************************************
		Menu Index:	320
        Function description: 开发-环境治理界面 [2]
        Entry data:     
        Return value: None
******************************************************************************/
void gui_develop_evi(void)
{
	printf("\33[2J");
	printf("\33[0;0H");
	printf("------环境治理------\r\n");
	printf("1.洪水防治\r\n");
	printf("2.赈灾救民\r\n");
	printf("3.兴办学业\r\n");	printf("\33[2J");
}

