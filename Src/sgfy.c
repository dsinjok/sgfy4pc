#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "game.h"
#include "interface.h"

#define DEBUG	0


int action_index_last=0;

int main(int argc, char **argv)
{
	char ch;
	int gui_index=0,menu_index=3;
	int action_index=0;

	struct player myplayer;
	system("stty -icanon");

	gui_about();
#if (DEBUG == 0)
	gui_comment();
	gui_start();
#endif
	ch = getchar();
	if(ch == '2')
	{
		if(load(&myplayer)) game_init(&myplayer);
	}
	else if(ch == '3')
	{
		gui_end();
		exit(0);
	}
	else
	{
		game_init(&myplayer);
	}

	while(1)
	{
		ch = getchar();
		if(ch == 'q')	break;
		if(ch == 's')	save(&myplayer);
		operateDecode	(&gui_index,&menu_index,&action_index,ch);
		interface		(gui_index,menu_index,&myplayer);
#if DEBUG
		debug			(&gui_index,&menu_index,&action_index,ch);
		printf("action_index_last:%d\r\n",action_index_last);
#endif
		action			(&gui_index,&menu_index,&action_index,&myplayer);
#if DEBUG
		debug			(&gui_index,&menu_index,&action_index,ch);
		printf("action_index_last:%d\r\n",action_index_last);
#endif
	}

	gui_end();

	return 0;
}