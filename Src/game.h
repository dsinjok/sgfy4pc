#ifndef	_GAME_H
#define	_GAME_H

#define	ACTION_TAX			210
#define	ACTION_PRODUCE		220
#define	ACTION_EDUCATE		230
#define	ACTION_VISIT		240
#define	ACTION_DEEL_M		251
#define	ACTION_DEEL_G		252
#define	ACTION_CONSCRIPTION	260
#define	ACTION_FARMING		311
#define	ACTION_METALLURGY	312
#define	ACTION_LOOKING		313
#define	ACTION_SIEGE		314
#define	ACTION_FLOOD		321
#define	ACTION_SALVATION	322
#define	ACTION_TEACHING		323
#define	ACTION_SEARCH		330
#define	ACTION_TRAINING		340

#define	METCH_MENU			0
#define	METCH_ACTION		1

struct player{
	char    name[64];
	int  	age;		//年龄

	int  	food;		//粮食
	int  	gold;		//黄金

	int  	zdl;		//战斗力
	int  	cc;			//统一度

	int  	edu;		//教育
	int  	evi;		//环境
	int  	con;		//信赖度
	int  	peo;		//人口

	int  	month;		//月份
	int  	city;		//城池

	int  	tax;		//税率
	int  	farm;		//农场开发率
	int  	stone;		//矿场开发率

	int  	jd;			//军队

	int  	graph;		//?
	int  	opeo;		//？
};

void game_init(struct player *p);
void save(struct player *p);
int load(struct player *p);
void operateDecode(int *gui_index, int *menu_index, int *action_index, char key);
void action(int *gui_index, int *menu_index,int *action_index,struct player *p);
void ending(int eindex, struct player *p);
void update(int *gui_index, int *menu_index,int *action_index,struct player *p);

void tax(int *gui_index, int *menu_index,int *action_index,struct player *p);
void produce(int *gui_index, int *menu_index,int *action_index,struct player *p);
void educate(int *gui_index, int *menu_index,int *action_index,struct player *p);
void visit(int *gui_index, int *menu_index,int *action_index,struct player *p);
void deel(int *gui_index, int *menu_index,int *action_index,struct player *p);
void conscription(int *gui_index, int *menu_index,int *action_index,struct player *p);
void farming(int *gui_index, int *menu_index,int *action_index,struct player *p);
void metallurgy(int *gui_index, int *menu_index,int *action_index,struct player *p);
void looking(int *gui_index, int *menu_index,int *action_index,struct player *p);
void siege(int *gui_index, int *menu_index,int *action_index,struct player *p);
void flood(int *gui_index, int *menu_index,int *action_index,struct player *p);
void salvation(int *gui_index, int *menu_index,int *action_index,struct player *p);
void teaching(int *gui_index, int *menu_index,int *action_index,struct player *p);
void search(int *gui_index, int *menu_index,int *action_index,struct player *p);
void training(int *gui_index, int *menu_index,int *action_index,struct player *p);

#endif
